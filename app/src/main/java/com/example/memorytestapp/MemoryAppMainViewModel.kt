package com.example.memorytestapp

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.memorytestapp.ui.screens.Screens
import com.example.memorytestapp.util.MemoryTestServerClient
import com.example.memorytestapp.util.Navigator
import com.example.memorytestapp.util.UrlBack
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MemoryAppMainViewModel: ViewModel() {
    private var request: Job? = null
    private var backDownload: Job? = null

    val navigator = Navigator()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        navigator.navigateTo(Screens.SPLASH_SCREEN)
        downloadAssets()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = MemoryTestServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> downloadAssets()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                downloadAssets()
                            }
                            else -> viewModelScope.launch {
                                navigator.setUrl("https://${splash.body()!!.url}")
                                navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else downloadAssets()
                } else downloadAssets()
            }
        } catch (e: Exception) {
            downloadAssets()
        }
    }

    private lateinit var tar: Target
    private fun downloadAssets() {
        if(backDownload?.isActive == true) {
            backDownload?.cancel()
        }
        backDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            tar = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        navigator.setBack(bitmap)
                        switchToGame()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlBack).into(tar)
        }
    }

    private fun switchToGame() {
        navigator.navigateTo(Screens.GAME_SCREEN)
    }

    private fun switchToErrorScreen() {
        navigator.navigateTo(Screens.ASSETS_LOADING_ERROR_SCREEN)
    }
}