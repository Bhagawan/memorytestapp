package com.example.memorytestapp.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.memorytestapp.ui.screens.ErrorScreen
import com.example.memorytestapp.ui.screens.Screens
import com.example.memorytestapp.ui.screens.SplashScreen
import com.example.memorytestapp.ui.screens.WebViewScreen
import com.example.memorytestapp.ui.screens.memoryTestScreen.MemoryTestScreen
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, navigator: Navigator, errorScreenAction : () -> Unit, orientationChange: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.ASSETS_LOADING_ERROR_SCREEN.label) {
            BackHandler(true) {}
            ErrorScreen(errorScreenAction)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { navigator.getUrl() })
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            MemoryTestScreen(navigator.getBack())
        }
    }
    LaunchedEffect("navigation") {
        navigator.navigationFlow.onEach {
            if(it == Screens.GAME_SCREEN) orientationChange()
            navController.navigate(it.label)
        }.launchIn(this)
    }
    navController.enableOnBackPressed(true)
}