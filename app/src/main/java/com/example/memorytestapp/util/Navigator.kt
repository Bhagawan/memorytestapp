package com.example.memorytestapp.util

import android.graphics.Bitmap
import com.example.memorytestapp.ui.screens.Screens
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class Navigator {
    private var url = ""
    private var back : Bitmap? =  null

    private val _navigationFlow = MutableSharedFlow<Screens>(extraBufferCapacity = 1)
    val navigationFlow = _navigationFlow.asSharedFlow()

    fun navigateTo(targetScreens: Screens) {
        _navigationFlow.tryEmit(targetScreens)
    }

    fun setUrl(newUrl: String) {
        url = newUrl
    }

    fun getUrl(): String = url

    fun setBack(newBack: Bitmap) {
        back = newBack
    }

    fun getBack(): Bitmap? = back
}