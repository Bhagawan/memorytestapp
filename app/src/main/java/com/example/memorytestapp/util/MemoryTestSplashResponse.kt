package com.example.memorytestapp.util

import androidx.annotation.Keep

@Keep
data class MemoryTestSplashResponse(val url : String)