package com.example.memorytestapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.memorytestapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer

class MemoryTestView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0

    companion object {
        const val STATE_SELECTION = 3
        const val STATE_ANIMATION = 0
        const val STATE_GAME = 1
        const val STATE_PAUSE = 2
        const val ANIMATION_LENGTH = 60
    }

    private var newCard = 1
    private var currAnimPos = 0
    private var selectedCard = -1
    private var selectionTimer = 0
    private var selectedWrong = false

    private val cardBitmaps = listOf(
        AppCompatResources.getDrawable(context, R.drawable.ic_child_play)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_field_horizontal)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_field_vertical)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_football)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_gate)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_glove)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_headkick)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_jersey)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_left_corner)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_player)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_referee)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_right_corner)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_score)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shirt)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shoe)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shorts)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_socks)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_team)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_trophy)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_whistle)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_winner)?.toBitmap()
    )

    private val cards = ArrayList<Card>()

    private var state = STATE_PAUSE

    private var mInterface: MemoryTestInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state != STATE_GAME) updateCards()
            drawCards(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    else -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    else -> {}
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    STATE_GAME -> checkClick(event.x, event.y)
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: MemoryTestInterface) {
        mInterface = i
    }

    fun restart() {
        currAnimPos = 0
        selectionTimer = 0
        cards.clear()
        addCard()
        selectedCard = -1
        state = STATE_GAME
    }

    //// Private

    private fun drawCards(c: Canvas) {
        val p = Paint()
        for(card in cards) {
            p.strokeWidth = 2.0f
            p.color = Color.WHITE
            val percent = if(currAnimPos < ANIMATION_LENGTH / 2) 100 - currAnimPos / (ANIMATION_LENGTH / 200.0f)
            else (currAnimPos - (ANIMATION_LENGTH / 2)) / (ANIMATION_LENGTH / 200.0f)
            val w = card.width * percent / 100.0f
            p.style = Paint.Style.FILL
            c.drawRoundRect(card.x - w / 2, card.y - card.height / 2, card.x + w / 2, card.y + card.height / 2, 2.0f, 2.0f, p)
            p.style = Paint.Style.STROKE
            p.color = if(selectedCard == cards.indexOf(card)) {
                p.strokeWidth = 16.0f
                if(selectedWrong) Color.RED else Color.GREEN
            } else Color.GRAY
            c.drawRoundRect(card.x - w / 2, card.y - card.height / 2, card.x + w / 2, card.y + card.height / 2, 2.0f, 2.0f, p)
            cardBitmaps[card.bitmapNumber]?.let { c.drawBitmap(it, null, Rect((card.x - w * 0.45f).toInt(), (card.y - card.height * 0.45f).toInt(), (card.x + w * 0.45f).toInt(), (card.y + card.height * 0.45f).toInt()), p) }
        }
    }

    private fun updateCards() {
        if(state == STATE_ANIMATION) {
            currAnimPos++
            if(currAnimPos == ANIMATION_LENGTH / 2) addCard()
            else if(currAnimPos > ANIMATION_LENGTH)  state = STATE_GAME
        } else if(state == STATE_SELECTION) {
            selectionTimer ++
            if(selectionTimer > 30) {
                if(selectedWrong) {
                    state = STATE_PAUSE
                    mInterface?.finish(cards.size)
                } else {
                    state = STATE_ANIMATION
                    currAnimPos = 0
                    selectedCard = -1
                }
            }
        }
    }

    private fun checkClick(x: Float, y: Float) {
        for(n in cards.indices) {
            if(x in (cards[n].x - cards[n].width / 2)..(cards[n].x + cards[n].width / 2) && y in (cards[n].y - cards[n].height / 2)..(cards[n].y + cards[n].height / 2)) {
                selectedWrong = newCard != n
                selectedCard = n
                selectionTimer = 0
                state = STATE_SELECTION
                break
            }
        }
    }


    private fun addCard() {
        val availableCards = cardBitmaps.indices.toCollection(ArrayList())
        for(card in cards) availableCards.remove(card.bitmapNumber)
        if(availableCards.isEmpty()) {
            state = STATE_PAUSE
            mInterface?.win()
        } else {
            val nCard = Card(availableCards.random())
            cards.add(nCard)
            cards.shuffle()
            newCard = cards.indexOf(nCard)
            placeCards()
        }
    }

    private fun placeCards() {
        var height = if(cards.size > 3) mHeight * 0.4f else mHeight * 0.9f
        val topRow = if(cards.size > 3) cards.size - (cards.size / 2) else cards.size
        var width = (mWidth / (topRow + (topRow + 1) * 0.1f)).coerceAtMost(height * 0.75f)
        val space = width * 0.1f
        width *= 0.9f
        height = width * 1.25f

        for(n in cards.indices) {
            cards[n].apply {
                this.width = width
                this.height = height
                val offset = if(n < topRow) (mWidth- (width + space) * topRow) / 2.0f - space
                else (mWidth- (width + space) * (cards.size - topRow)) / 2.0f - space
                x = offset + space + width* 0.5f + if(n < topRow) (width + space) * n  else (width + space) * (n - topRow)
                y = if(topRow < cards.size) {
                    if(n < topRow) mHeight / 4.0f
                    else mHeight * 0.75f
                } else mHeight / 2.0f
            }
        }
    }

    interface MemoryTestInterface {
        fun finish(score: Int)
        fun win()
    }
}