package com.example.memorytestapp.view

data class Card(val bitmapNumber: Int, var x: Float = 1.0f, var y: Float = 1.0f, var width: Float = 1.0f, var height: Float = 1.0f)