package com.example.memorytestapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Grey_light = Color(0xFF6B6B6B)
val Grey = Color(0xFF4B4B4B)