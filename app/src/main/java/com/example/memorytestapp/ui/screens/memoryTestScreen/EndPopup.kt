package com.example.memorytestapp.ui.screens.memoryTestScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.memorytestapp.R
import com.example.memorytestapp.ui.theme.Grey

@Composable
fun EndPopup(points: Int, onClick : () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClick() }, contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .wrapContentSize()
            .background(color = Grey, shape = RoundedCornerShape(Dp(15.0f)))
            .padding(Dp(20.0f)), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Text(stringResource(id = R.string.end_header), fontSize = TextUnit(25.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center)
            Text(points.toString(), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center)
            Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(id = R.string.desc_reset), modifier = Modifier
                .size(Dp(100.0f))
                .padding(Dp(10.0f)))
        }
    }
}
