package com.example.memorytestapp.ui.screens.memoryTestScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MemoryTestViewModel: ViewModel() {
    private val _introPopup = MutableStateFlow(true)
    val introState = _introPopup.asStateFlow()

    private val _endPopup = MutableStateFlow(true)
    val endState = _endPopup.asStateFlow()
    var points = 0

    private val _winPopup = MutableStateFlow(true)
    val winState = _winPopup.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    fun finish(points: Int) {
        this.points = points
        _endPopup.tryEmit(true)
    }

    fun win() {
        _winPopup.tryEmit(true)
    }

    fun restart() {
        _introPopup.tryEmit(false)
        _endPopup.tryEmit(false)
        _winPopup.tryEmit(false)
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }
}