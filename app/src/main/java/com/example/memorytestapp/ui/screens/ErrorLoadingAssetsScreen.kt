package com.example.memorytestapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.memorytestapp.R
import com.example.memorytestapp.ui.theme.Grey_light
import com.example.memorytestapp.ui.theme.Shapes

@Composable
fun ErrorScreen(onClick: ()-> Unit ) {
    Column(
        modifier = Modifier.fillMaxSize().clickable { onClick() },
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Text(text = stringResource(id = R.string.error), fontSize = TextUnit(15.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.Black,
            modifier = Modifier.padding(horizontal = Dp(50.0f), vertical = Dp(30.0f)))

        Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(
            id = R.string.desc_reset), Modifier.background(Grey_light, Shapes.medium))
    }
}