package com.example.memorytestapp.ui.screens.memoryTestScreen

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.memorytestapp.view.MemoryTestView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun MemoryTestScreen(back: Bitmap?) {
    val viewModel = viewModel<MemoryTestViewModel>()
    Box(modifier = Modifier.fillMaxSize()) {
        back?.let {
            Image(it.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        }
        AndroidView(factory = { MemoryTestView(it) },
            modifier = Modifier.fillMaxSize(),
            update = { view ->
                viewModel.restartFlow.onEach { view.restart() }.launchIn(viewModel.viewModelScope)
                view.setInterface(object : MemoryTestView.MemoryTestInterface {
                    override fun finish(score: Int) {
                        viewModel.finish(score)
                    }
                    override fun win() {
                        viewModel.win()
                    }
                })
            })

        val introPopup = viewModel.introState.collectAsState(true)
        val endPopup = viewModel.endState.collectAsState(false)
        val winPopup = viewModel.winState.collectAsState(false)

        if(introPopup.value) IntroPopup(viewModel::restart)
        else if( endPopup.value) EndPopup(viewModel.points, viewModel::restart)
        else if( winPopup.value) WinPopup(viewModel::restart)
    }
}